#!/usr/bin/env python
import sys
import Adafruit_DHT

from dht22_datalogger import DHT22_DataLogger

# example:  sudo ./main.py 22 4 1 30 0 1 0 livingroom
# ssh pi@192.168.0.107
# scp source_file_path destination_file_path
# scp enviro_data_logger.py pi@192.168.1.107:~/sensors
# $ scp ~/my_local_file.txt user@remote_host.com:/some/remote/directory

# rsync rsync -av DHT22 pi@192.168.0.110:~/sensors/

DATABASE = 'lab01.db'
LOGDATA_ENDPOINT = 'http://192.168.0.101:8888/rpi_sensor/api/v1.0/logdata'

def main():

    # Parse command line parameters.
    sensor_args = {'11': Adafruit_DHT.DHT11,
                   '22': Adafruit_DHT.DHT22,
                   '2302': Adafruit_DHT.AM2302}

    # TODO add further validation here ...
    if sys.argv[1] in sensor_args:
        sensor = sensor_args[sys.argv[1]]
        pin = sys.argv[2]
        sensorid = sys.argv[3]
        sampletime = float(sys.argv[4])
        localstore = sys.argv[5]
        verbose = sys.argv[6]
        liveplot = sys.argv[7]
        location = sys.argv[8]
    else:
        print ('usage: sudo ./env_data_log.py [11|22|2302] GPIOpin# [4] sensorid<int> sampletime<float> verboseflag[0,1,2] liveplot [1,0] location<string>')
        sys.exit(1)

    # instantiate a data logger
    dl = (DHT22_DataLogger(DATABASE, LOGDATA_ENDPOINT, sensor,pin, sensorid, sampletime, localstore, verbose, liveplot, location))

    # debug cli arguments and object attributes
    if verbose >= 1:
        print(sys.argv)
        print(repr(dl))

    dl.CaptureData()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        GPIO.cleanup()
    # finally:
    #     GPIO.cleanup()
