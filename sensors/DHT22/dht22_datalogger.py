import sys
import time
import Adafruit_DHT
import RPi.GPIO as GPIO
import sqlite3
import datetime
import urllib2
import json
import plotly.plotly as py
import random

class DHT22_DataLogger:

    # constructor
    def __init__(self, db_name, srvc_endpoint, sensor, pin, sensorid, sampletime, localstore, verbose, liveplot, location):
        self.DATABASE = db_name
        self.LOGDATA_ENDPOINT = srvc_endpoint
        self.__sensor = sensor
        self.__pin = pin
        self.__sampletime = sampletime
        self.__localstore = localstore
        self.__verbose = verbose
        self.__liveplot = liveplot
        self.__location = location
        self.__sensor_id = sensorid

    def __repr__(self):
        return ("<__repr__ DHT22_DataLogger DATABASE: %s URL: %s Sensor: %s Pin: %s SampleTime: %s UseLocalStore: %s Verbose: %s UseLivePlot: %s Location:%s Sensor_id: %s>" % (str(self.DATABASE), str(self.LOGDATA_ENDPOINT), str(self.GetSensor()), str(self.GetPin()), str(self.GetSampleTime()), str(self.UseLocalStore()), str(self.GetVerbose()), str(self.GetLiveplot()), str(self.GetLocation()), str(self.GetSensorid())))

    def __str__(self):
        return ("<__str__ DHT22_DataLogger DATABASE: %s URL: %s Sensor: %s Pin: %s SampleTime: %s UseLocalStore: %s Verbose: %s UseLivePlot: %s Location: %s Sensor_id: %s>" % (str(self.DATABASE), str(self.LOGDATA_ENDPOINT), str(self.GetSensor()), str(self.GetPin()), str(self.GetSampleTime()), str(self.UseLocalStore()), str(self.GetVerbose()), str(self.GetLiveplot()), str(self.GetLocation()), str(self.GetSensorid())))

    # getters
    def GetSensor(self):
        return self.__sensor

    def GetPin(self):
        return self.__pin

    def GetSampleTime(self):
        return float(self.__sampletime)

    def UseLocalStore(self):
        return self.__localstore

    def GetVerbose(self):
        return self.__verbose

    def GetLiveplot(self):
        return self.__liveplot

    def GetLocation(self):
        return self.__location

    def GetSensorid(self):
        return int(self.__sensor_id)

    def connect_db(self):
        return sqlite3.connect(self.DATABASE)

    # methods and functions
    def getSensorData(self):
        #humidity = random.uniform(0.0, 100.0)
        #temperature = random.uniform(-20.0, 80.0)
        humidity, temperature = Adafruit_DHT.read_retry(self.GetSensor(), self.GetPin())
        return (humidity, temperature)

    # post request to service for remote database
    def post_to_service(self, temperature, humidity, sensorid, location):
        self.logmessage('posting to service.')
        print('posting')
        # Prepare the data
        payload = { 'temp_c': temperature, 'humidity': humidity, 'location': location, 'sensorid': sensorid }
        url = self.LOGDATA_ENDPOINT
        data = json.dumps(payload)
        headers = {'content-type': 'application/json'}

        self.logmessage(data)

        # Send HTTP POST request
        request = urllib2.Request(url, data, headers=headers)
        try:
            response = urllib2.urlopen(request)
        except (urllib2.HTTPError):
            print ('The server returned an error.')
            #print ('Error code - ', e.code)
        except (urllib2.URLError):
            print ('Could not reach server.')
            #print ('Reason - ', e.reason)
        else:
            print (response.read())


    # direct insert into local database
    def log_to_db(self, temperature, humidity, sensorid, location):
        logmessage('logging to local db')
        temp_f = self.c_to_f(temperature)
        try:
            conn = self.connect_db()
            curs = conn.cursor()

            msg = ('Logging to DB: Temp_Celsius={0: 0.1f}*C '
               'Temp_Fahrenheit={1: 0.1f}*F Humidity={2: 0.1f}% '
               .format(temperature, temp_f, humidity))
            self.logmessage(msg)

            dt = [temperature, temp_f, humidity, sensorid, location]
            curs.execute('INSERT INTO tblTempHumidityLog(temp_c, temp_f, humidity, sensor_id, location) values(?, ?, ?, ?, ?)', dt)

            # commit the changes
            conn.commit()
        except Exception:
            self.logmessage('Database connection error:  Data not logged')
        finally:
            conn.close()


    def CaptureData(self):
        if self.GetLiveplot == 1:
            self.LogToPlotly()
        else:
            while True:
                # get data
                humidity, temperature = self.getSensorData()
                if humidity is not None and temperature is not None:
                    if self.GetVerbose >= 1:
                        message = "%s C %s F %s Humidity %s SensorID: %s" % (str(temperature), str(self.c_to_f(temperature)), str(humidity), self.GetLocation(), str(self.GetSensorid()))
                        self.logmessage(message)
                    #if self.UseLocalStore == '1':
                    #    self.log_to_db(temperature, humidity, self.GetSensorid(), self.GetLocation())
                    #elif self.UseLocalStore == '0':
                    self.post_to_service(temperature, humidity, self.GetSensorid(), self.GetLocation())
                time.sleep(self.GetSampleTime())

    #utility functions
    def c_to_f(self, deg_c):
        return (9.0/5.0 * float(deg_c)) + 32

    def logmessage(self, msg):
        utc_dt = str(datetime.datetime.utcnow())
        now_dt = str(datetime.datetime.now())
        msg_str = "UTC: {0}  Now: {1}: {2}".format(utc_dt, now_dt, msg)
        print(msg_str)

    def LogToPlotly(self):
        self.logmessage("Plotting to plot.ly")
        with open('./config.json') as config_file:
            #plotly_user_config = simplejson.load(config_file)
            plotly_user_config = json.load(config_file)

            py.sign_in(plotly_user_config["plotly_username"],
                       plotly_user_config["plotly_api_key"])

            url = py.plot([
                {
                    'x': [], 'y': [], 'type': 'scatter',
                    'stream': {
                        'token': plotly_user_config['plotly_streaming_tokens'][0] ,
                        'maxpoints': 200
                    }
                }], filename='Raspberry Pi Streaming Example Values')

            self.logmessage("View your streaming graph here: ", url)

            stream = py.Stream(plotly_user_config['plotly_streaming_tokens'][0])
            stream.open()


            while True:
                # get data
                #humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
                humidity, temperature = self.getSensorData()

                # write to plot.ly stream
                if liveplot == '1':
                    temp_f = self.c_to_f(temperature)
                    stream.write({'x': datetime.datetime.now(), 'y': temperature})

                time.sleep(float(self.GetSampleTime()))
