from flask import Flask
app = Flask(__name__)

import sys
import sqlite3
import json
from flask import g

from flask import jsonify
from flask import request, json
from flask import abort
from flask import make_response
from flask import render_template
import datetime
import logging

# DATABASE = 'lab01.db'
DATABASE = '../lab01.db'
from flask import request


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


def connect_db():
    return sqlite3.connect(DATABASE)


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    if hasattr(g, 'db'):
        g.db.close()


def query_db(query, args=(), one=False):
    cur = g.db.execute(query, args)
    rv = [dict((cur.description[idx][0], value)
               for idx, value in enumerate(row)) for row in cur.fetchall()]
    return (rv[0] if rv else None) if one else rv


# @app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.route('/rpi_sensor/api/v1.0/taillog', methods=['GET'])
def getlogs():
    app.logger.info('flask_service informing')
    app.logger.warning('flask_service warning')
    app.logger.error('flask_service error')

    return ("TODO: check your logs\n")


@app.route('/rpi_sensor/api/v1.0/shutdown', methods=['POST'])
def shutdown():
    shutdown_server()
    return 'Server shutting down...'

@app.route("/rpi_sensor/api/v1.0/logdata", methods=['POST'])
def logData():
    datapoint = request.get_json()
    print(datapoint)

    try:
        temp_c = float(datapoint['temp_c'])
        humidity = float(datapoint['humidity'])
        location = datapoint['location']
        sensorid = datapoint['sensorid']
        temp_f = c_to_f(temp_c)
        dt = [temp_c, temp_f, humidity, location, sensorid]
        fields =  ['temp_c', 'temp_f', 'humidity', 'location', 'sensor_id']
        rowid = db_insert('tblTempHumidityLog', fields, dt)
        datapoint['id'] = rowid
        return  json.dumps({'ID':rowid})
    except Exception:
        logmessage('Database connection error:  Data not logged')
        return  json.dumps({'ID':'-1'})


@app.route("/rpi_sensor/api/v1.0/temp_humidity", methods=['GET'])
def getenvirologdata():

    # 0|ID|INTEGER|0||1
    # 1|logdate_utc|TIMESTAMP|0|CURRENT_TIMESTAMP|0
    # 2|logdate_local|DATE DEFAULT (datetime('now','localtime')),
    # 3|temp_c|NUMERIC|1||0
    # 4|temp_f|NUMERIC|1||0
    # 5|humidity|NUMERIC|1||0
    # 6|location|TEXT|0||0

    min_ago = request.args.get('ago', '')

    getrecords_sql = ( "SELECT CAST(strftime('%s', logdate_local) AS INT) * 1000 as unixepoch, * FROM tbltemphumiditylog WHERE logdate_local BETWEEN datetime('now', '-" + str(min_ago) + " minutes','localtime') AND datetime('now', 'localtime') ORDER BY logdate_utc DESC;" )
    json_output = json.dumps(query_db(getrecords_sql))
    return json_output


@app.route('/temp_humidity/recent')
@app.route('/about')
@app.route('/taillog')
@app.route('/')
def basic_pages(**kwargs):
    return make_response(open('/home/deise/raspberry_pi/sensors/sensor_srvc/templates/index.html').read())

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404
    app.logger.error('Error 404: ' + request.url)
    # return resp
    return render_template('404.html'), 404

def c_to_f(deg_c):
    return (9.0/5.0 * float(deg_c)) + 32

def logmessage(msg):
    utc_dt = str(datetime.datetime.utcnow())
    now_dt = str(datetime.datetime.now())
    msg_str = "UTC: {0}  Now: {1}: {2}".format(utc_dt, now_dt, msg)
    print(msg_str)

def db_insert(table, fields=(), values=()):
    # g.db is the database connection
    cur = g.db.cursor()
    query = 'INSERT INTO %s (%s) VALUES (%s)' % (
        table,
        ', '.join(fields),
        ', '.join(['?'] * len(values))
    )
    print(query, values)
    cur.execute(query, values)
    g.db.commit()
    id = cur.lastrowid
    cur.close()
    return id

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8888, debug=True)
