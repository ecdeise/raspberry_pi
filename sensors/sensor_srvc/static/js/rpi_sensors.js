// Copyright (c) 2015 by Erich C. Deise. All Rights Reserved.

//deise@D1:~/raspberry_pi/sensors$ scp -v -r sensor_srvc/ pi@192.168.1.3:~/sensors


angular.module('rpi_ng', ['ngRoute', 'ngTable'])
.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider, $http) {
    'use strict';
    $routeProvider

    .when('/temp_humidity/recent', {
        templateUrl: '/static/partials/temp_humidity.html',
        controller: 'temp_humidity_recentController'
    })
    .when('/taillog', {
        templateUrl: '/static/partials/taillog.html',
        //controller: 'LogController'
    })
    .when('/shutdown', {
        templateUrl: '/static/partials/landing.html',
        controller: 'KillController'
    })
    .when('/about', {
        templateUrl: '/static/partials/about.html',
        //controller: 'IndexController'
    })
    .when('/', {
        templateUrl: '/static/partials/landing.html',
        //controller: 'IndexController'
    })
    .otherwise({
        redirectTo: '/'
    })
    ;

    $locationProvider.html5Mode(true);
}])


.factory('Datafactory', ['$http', '$q', '$routeParams', function($http, $q, $routeParams) {
  function getdatapoints(timelimit) {
    var deferred = $q.defer();
    $http
    .get('/rpi_sensor/api/v1.0/temp_humidity', {params:{ago:timelimit}})
    .success(function(data, status, headers, config) {
      deferred.resolve(data);
    }).error(function(data, status, headers, config) {
        console.log('error retrieving data from sqlite');
      deferred.reject(data);
    });
    return deferred.promise;
  }
    return {getdatapoints: getdatapoints};

}]) //end factory

.controller('LogController',['$scope', function ($scope, ngTableParams) {
  $scope.logs = 'check the logs...';

}]) //end LogController


.controller('temp_humidity_recentController',['Datafactory', '$scope', 'ngTableParams', '$interval', function (Datafactory, $scope, ngTableParams, $interval) {
  $scope.temp_toggle = true;
  $scope.datapoints = [];

$scope.timeoptions = [
    { label: 'n/a', value: -99 },
    { label: '1 min', value: 1 },
    { label: '5 mins', value: 5 },
    { label: '15 mins', value: 15 },
    { label: '1 hour', value: 60 },
    { label: '8 hours', value: 480 },
    { label: '1 day', value: 1440 },
    { label: '1 week', value: 10080 },
    { label: '2 weeks', value: 20160 },
    { label: '1 Month', value: 43200 }
    ];


function getdata(timelimit) {

  console.log('getdate called...');

  var datalist_CH = [];
  var datalist_FH = [];

  var HUMID_COLOR = "#89A3BE";
  var CELSIUS_COLOR = "#FF9900";
  var F_COLOR = "#CC6699";

  var intrvl;

  Datafactory.getdatapoints(timelimit)
     .then(function(data) {
      $scope.datapoints = data;
      var datalist_all = [];
      var datalist_c = [];
      var datalist_f = [];
      var datalist_h = [];
      for (var dobj in data) {
          var co = data[dobj];
          var timestamp = new Date(co.unixepoch).getTime();
          datalist_c.push([timestamp, co.temp_c]);
          datalist_f.push([timestamp, co.temp_f]);
          datalist_h.push([timestamp, co.humidity]);
      };

        datalist_all = [{ data:datalist_f, label:"Fahrenheit ", yaxis:3, color: F_COLOR, lines:{show:true}, points:{show:true}}
                    ,{ data:datalist_c, label:"Celsius ", yaxis:2, color: CELSIUS_COLOR, lines:{show:true}, points:{show:true}}
                    ,{ data:datalist_h, label:"Humidty ", yaxis:1, color: HUMID_COLOR ,lines:{show:true}, points:{show:true}}
                    ];

        datalist_CH = [{ data:datalist_c, label:"Celsius ", yaxis:2, color: CELSIUS_COLOR, lines:{show:true}, points:{show:true}}
                    ,{ data:datalist_h, label:"Humidty ", yaxis:1, color: HUMID_COLOR, lines:{show:true}, points:{show:true}}];

        datalist_FH = [{ data:datalist_f, label:"Fahrenheit ", yaxis:3, color: F_COLOR, lines:{show:true}, points:{show:true}}
                    ,{ data:datalist_h, label:"Humidty ", yaxis:1, color: HUMID_COLOR, lines:{show:true}, points:{show:true}}];

        $scope.data = datalist_all;

     });

     $scope.$watch('temp_toggle', function(){
         if ($scope.temp_toggle === false) {
             $scope.data = datalist_CH;
         } else {
             $scope.data = datalist_FH;
         }
         $scope.toggleText = $scope.temp_toggle ? 'Celsius' : 'Fahrenheit';
     });

 }


     if (angular.isUndefined($scope.timestamp)) {
         $scope.tableviz = true;
         $scope.tabletoggleText = 'Show';
     };

     if (angular.isUndefined($scope.updateInterval)) {
         $scope.updateInterval = 30;
         intrvl = $scope.updateInterval * 1000;
     } else {
         intrvl = $scope.updateInterval * 1000;
     };


    // show or hide data in tabular form
    $scope.table_toggle = function(){
        $scope.tableviz = !$scope.tableviz;
        $scope.tabletoggleText = $scope.tableviz ? 'Show' : 'Hide';
    };

    // time series change
    $scope.timechange = function() {
        $scope.data = '';
        getdata($scope.timelimit.value);
    };

    // manual update ....
    $scope.updatechart = function() {
        $scope.data = '';
        getdata($scope.timelimit.value);
    };

    // update interval changed
    $scope.intervalchange = function() {
        intrvl = $scope.updateInterval * 1000;
        console.log('interval change');
    };

    // interval update
    $interval(function(){
      console.log(intrvl + ' milliseconds');
      getdata($scope.timelimit.value);
    },intrvl);


}]) //end temp_humidity_recentController



.controller('KillController', ['$scope', function($scope){
    var url = 'http://127.0.0.1:8888/rpi_sensor/api/v1.0/shutdown';
    $http.get(url)
       .then(function(res){
          $scope.kill = res.data;
        });
}]) //end kill switch


.directive('chart', function(){
    return{
        restrict: 'E',
        link: function(scope, elem, attrs){

            var chart = null,
            opts  = {

            xaxis: {
                mode: "time",
                timeformat: "%H:%M:%S",
                //labelWidth: "40",
                axisLabel: "Time",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 10,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 3
            },


            yaxes:[{
             position: "left",
                color: "black",
                axisLabel: "Humidity",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 3,
                tickFormatter: function(v,axis){return v.toFixed(axis.tickDecimals) + "%";}
            }, {
                position: "right",
                color: "black",
                axisLabel: "Celsius",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 3,
                tickFormatter: function(v,axis){return v.toFixed(axis.tickDecimals) + "&deg";}
            }, {
                position: "right",
                color: "black",
                axisLabel: "Fahrenheit",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 3,
                tickFormatter: function(v,axis){return v.toFixed(axis.tickDecimals) + "&deg";}
            }],

            grid: {
                clickable: true,
                hoverable: true,
                autoHighlight: true,
                backgroundColor: null,
                borderWidth: 1,
                color: "#666",
                labelMargin: 10,
                axisMargin: 1,
                mouseActiveRadius: 10,
                minBorderMargin: 5
            },
            // series: {
            //     lines: {
            //         show: true,
            //         lineWidth: 2,
            //         steps: false,
            //         //fill: true
            //     },
            //     points: {
            //         show:true,
            //         radius: 4,
            //         symbol: "circle",
            //         //fill: true
            //     }
            // },
            tooltip: true,
            tooltipOpts: {
                content: "Time: %x</br>Data: %y",
                xDateFormat: "%m/%d %H:%M:%S",
                shifts: {
                    x: 20,
                    y: 0
                },
                defaultTheme: true
            },

            // legend: {
            //     //noColumns: 0,
            //     position: "ne"
            // },

            //legend: { show: true, container: '#legendholder' }},
            legend: {   show: true,
                        //noColumns: 0,
                        //container: $('#legendholder'),
                        position: "se",
                        //labelBoxBorderColor:'black',
                        //margin: 2,
                        //padding: 2,
                        //backgroundColor: '#CCCCFF',
                        backgroundOpacity: 0.4
                    },

            //colors: ["#0892cd"],
            //shadowSize: 0

            };

            scope.$watch(attrs.ngModel, function(v){
                if(!chart){
                    if (v) {
                        chart = $.plot(elem, v , opts);
                        elem.show();
                    }


                }else{
                    chart.setData(v);
                    chart.setupGrid();
                    chart.draw();
                }
            });
        }
    };
})


;//end of file
