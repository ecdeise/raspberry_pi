# raspberry_pi
Raspberry Pi Sensors Project - monitor humidty and temperature using python3/flask/angular/sqlite/Flot

A little something the boy and I have been working on together ... a first foray into flask, angular and flot a work in progress.  

TODO: provide wiring diagram

1)  main.py is a script to be run on the raspberry pi (connected to DHT humidty and temperature sensor).
Data from that processes is written to a sqlite3 database either local to the pi or POSTED to a service for
remote store.

TODO: delete older records so that that table stays manageable in size. cron job

Usage:       sudo ./main.py 22 4 1 15 1 1 livingroom
parameters :
            sensor = DHT sensory
            pin = GPIO
            sensorid = id of the sensor
            sampletime = float - how often to sample in seconds
            localstore = 1 use local DB 0 POST to service
            verbose = laconic, chatty
            liveplot = send data to plotly
            location = where is the pi recording data


2)  sensor_srvc is a python/flask webservice and ui/visualization for the data.  

Usage: python3 app_server.py

Bits and pieces of the project can be found scattered about in numerous tutorials -
